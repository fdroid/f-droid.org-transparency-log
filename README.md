
# Binary Transparency log for https://f-droid.org

This is a log of the signed app indexes downloaded from
https://f-droid.org/repo/ This is stored in a git repo, which
serves as an imperfect append-only storage mechanism.  People can then
check that any file that they received from that F-Droid repository
was a publicly released file.

For more info:
* https://wiki.mozilla.org/Security/Binary_Transparency

This is generated as part of the official publishing workflow using `fdroid
btlog`.  The canonical location for this data is:

* https://gitlab.com/fdroid/f-droid.org-transparency-log
